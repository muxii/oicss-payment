import React, { useState } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

const ErrorModal = ({ isOpen, open, close, message }) => {
  return (
    <div>
      {/* <Button variant="primary" onClick={open}>
        Launch demo modal
      </Button> */}

      <Modal show={isOpen} onHide={close} centered>
        <Modal.Header closeButton>
          <Modal.Title>Error Modal</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={close}>
            ok
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default ErrorModal;
