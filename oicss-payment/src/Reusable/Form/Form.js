import React, { useState, useEffect } from "react";
import { useForm, useFieldArray } from "react-hook-form";
import "./Form.css";
import es from "date-fns/locale/es";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment'


export const Form = props => {
  const [show, setShow] = useState(false);
  const {
    register,
    handleSubmit,
    setValue,
    watch,
    control,
    errors
  } = useForm();
  const [date, setDate] = useState(new Date());
  const [newDate,setNewDate]=useState(null)
  const [storeDate,setStoreDate]=useState(null)
  const [vehicleno,setVehicleNo]=useState('');
  const [ownerNo,setOwnerNo]=useState('')
  const [ownerName,setOwnerName]=useState('')


  const name = watch("name");
  const ExampleCustomInput = ({ placeholderText,value, onClick }) => (
    <button className="example-custom-input btn-block" onClick={onClick}>
     
      <i
        style={{ padding: "5px", marginRight: "150px" }}
        class="fa fa-calendar"
        aria-hidden="true"
      ></i>
      
      {!value?
    placeholderText:value  
    }
    </button>
  );

  const onSubmit = data => {
    console.log(data);
  };

  const showFunction = () => {
    setShow(false);
    console.log("hello Function");
  };

  const handleChange = date => {
    const momentDate = moment().format("DD/MM/YYYY, h:mm a")
    console.log(date.getDate()+" "+date.getMonth()+" "+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes());
    setStoreDate(date.getDate()+"-"+date.getMonth()+"-"+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes())
    
    
    // const newdate = (date.getDate()+" "+date.getMonth()+" "+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes())
    // setNewDate(newdate)
    setNewDate(date);
    
    // if (newDate === null) 
    // setNewDate(date.getDate()+" "+date.getMonth()+" "+date.getFullYear()+" "+date.getHours());
    

  };
  console.log(newDate)
  console.log(storeDate)
  console.log(vehicleno)
  console.log(ownerNo)
  console.log(ownerName)
  
  const formdata={
    "VRNumber":vehicleno,
    "name":ownerName,
    "contact_no":ownerNo,
    "entryTime":storeDate
  }
  
  sessionStorage.setItem("formdata",JSON.stringify(formdata))
  console.log(typeof(vehicleno),ownerName,ownerNo)
  return (
    <form className="" onSubmit={handleSubmit(onSubmit)}>
      <div>
        {props.data && props.data ? (
          <>
            <div>
              <label for="name">Booking Number</label>
            </div>
            <div class="input-group mb-3">
              <input
                class="form-control"
                type="password"
                id="forminput"
                name="Password"
                disabled
              />
            </div>

            {errors.Password && errors.Password.type === "required" && (
              <div
                className="container"
                style={{ margin: "10px", color: "red" }}
              >
                <div className="row">
                  <i
                    class="fa fa-exclamation-triangle"
                    aria-hidden="true"
                    style={{ margin: "5px" }}
                  ></i>
                  <span>Booking Number cannot be Empty</span>
                </div>
              </div>
            )}
          </>
        ) : (
          ""
        )}
      </div>

      <div className="row justify-content-end">
        <div className="col-12 ">
          <div>
            <label for="name">Vehicle Number</label>
          </div>
          <div>
            <div class="input-group mb-3">
              <input
                type="number"
                
                id="forminput"
                name="vehicleno"
                ref={register({ required: true, maxLength: 30 })}
                onChange={(e)=>setVehicleNo(e.target.value)}
                // className="form-control"
                style={{ width: "100%", height: "40px" }}
                aria-label="Username"
                aria-describedby="basic-addon1"
              />
            </div>

            {errors.vehicleno && errors.vehicleno.type === "required" && (
              <div
                className="container"
                style={{ margin: "10px", color: "red" }}
              >
                <div className="row">
                  <i
                    class="fa fa-exclamation-triangle"
                    aria-hidden="true"
                    style={{ margin: "5px" }}
                  ></i>
                  <span>vehicle No can not be Empty</span>
                </div>
              </div>
            )}
          </div>
          {!props.data && (
            <>
              <div>
                <label for="name">In Date</label>
              </div>

              <div class="input-group mb-3">
                <div className="customDatePickerWidth">
                  <DatePicker
                
                    selected={newDate}
                    onChange={handleChange}
                    dateFormat="dd/MM/yyyy "
                    showTimeInput
                    closeOnScroll={true}
                    borderColor="red"
                    
                    customInput={<ExampleCustomInput placeholderText="Select Date"/>}
                  />
                </div>
              </div>
            </>
          )}

          {/* <div id="more-info">
            <div style={{ color: "#00BBDC" }}>Enter Details</div> */}
            {show ? (
              <>
              <div id="more-info" onClick={() => setShow(false)}>
              <div style={{ color: "#00BBDC" }}>Enter Details</div>
              <div onClick={() => setShow(false)}>
                <i class="fa fa-minus"></i>
              </div>
              </div>
                         
                         <div>
                           <label for="name">Vehicle Owner Name </label>
                         </div>
                         <div class="input-group mb-3">
                           <input
                             class="form-control"
                             type="text"
                             id="forminput"
                            //  placeholder={props.data ? "Username" : ""}
                             name="vehicleownername"
                             onChange={(e)=>setOwnerName(e.target.value)}
                             ref={register({ required: true, maxLength: 30 })}
                             disabled={props.data ? true : false}
                           />
                         </div>
           
                         {errors.vehicleownername && errors.vehicleownername.type === "required" && (
                           <div
                             className="container"
                             style={{ margin: "10px", color: "red" }}
                           >
                             <div className="row">
                               <i
                                 class="fa fa-exclamation-triangle"
                                 aria-hidden="true"
                                 style={{ margin: "5px" }}
                               ></i>
                               <span>Vehicle Owner Name cannot be Empty</span>
                             </div>
                           </div>
                         )}
           
                         <div>
                           <label for="name">Vehicle Owner Number </label>
                         </div>
                         <div class="input-group mb-3">
                           <input
                             class="form-control"
                             type="number"
                             id="forminput"
                             name="vehicleownernumber"
                             onChange={(e)=>setOwnerNo(e.target.value)}
                             placeholder={props.data ? "Vehicleowner" : ""}
                             ref={register({ required: true, maxLength: 30 })}
                             disabled={props.data ? true : false}
                           />
                         </div>
           
                         {errors.vehicleownernumber && errors.vehicleownernumber.type === "required" && (
                           <div
                             className="container"
                             style={{ margin: "10px", color: "red" }}
                           >
                             <div className="row">
                               <i
                                 class="fa fa-exclamation-triangle"
                                 aria-hidden="true"
                                 style={{ margin: "5px" }}
                               ></i>
                               <span>Vehicle Owner Number cannot be Empty</span>
                             </div>
                           </div>
                         )}
                      
           </>
            ) : (
              <div id="more-info" onClick={() => setShow(true)}>
            <div style={{ color: "#00BBDC" }}>Enter Details</div>
              <div onClick={() => setShow(true)}>
                <i class="fa fa-plus"></i>
              </div>
              </div>
            )}
          </div>
          {show ? (
           <></>          ) : (
            ""
          )}
        </div>
      {/* </div> */}
    </form>
  );
};
