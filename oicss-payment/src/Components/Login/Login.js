import React, { Component } from "react";
import "./Login.css";
import { LoginHooks } from "../useHooks/Login";
import { ErrorModal } from "../../Reusable/ErrorModal/ErrorModal";
export default class Login extends Component {
  constructor(props) {
    super(props);
  }
  //   _errorFunction = value => {
  //     console.log(value);
  //     this.setState({ error: value });
  //   };
  render() {
    // const { error } = this.state;
    return (
      <>
        {/* {this.state.error ? (
          <div
            className="modal fade"
            id="exampleModal"
            tabindex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <ErrorModal />
          </div>
        ) : (
          ""
        )} */}
        <div className="login container">
          <div className="row justify-content-center">
            <div className="col-lg-4 col-sm-12 col-md-12">
              <div className="row justify-content-center">
                <img
                  src={require("../../Assets/otoparkLogo.png")}
                  alt="#otopark"
                />
              </div>
              <div className="row justify-content-center">
                <h3 className="welcometext">Welcome Ranger!</h3>
              </div>
              <LoginHooks />

              <div className="request-text row justify-content-center">
                Request Password
              </div>
            </div>
          </div>
        </div>
        <footer
          style={{
            height: "300px",
            width: "100%",
            display: "flex",
            alignItems: "flex-end"
          }}
        >
          <img
            style={{ width: "100%", height: "auto" }}
            src={require("../../Assets/mumbai.svg")}
          />
        </footer>
      </>
    );
  }
}
